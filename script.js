let myPokemon = {
	name:`meowth`,
	level: 3,
	health: 100,
	attack: 50,
	pound: function () {
		console.log(`This Pokemon tackled targetPokemon`);
		console.log(`targetPokemon's health is now reduced to
			targetPokemonHealth`);
	},
	
	faint: function () {
		console.log(`Pokemon fainted`);
	}
}

console.log(myPokemon);

function Pokemon (name, lvl, hp) {
	this.name   = name;
	this.level  = lvl;
	this.health = hp * 2;
	this.attack = lvl;
	this.pound  = function(target){
		target.health -= this.attack
		if(target.health <= 0) {
			target.faint()
		} else {
			console.log(`${this.name} pound ${target.name}`);
			console.log(`${target.name}'s health now reduced to ${target.health}`)
		}
	};
	this.faint = function(){
		console.log(`${this.name} fainted`);
			
	}
}

let treecko = new Pokemon(`treecko`, 3, 50);
let mew = new Pokemon(`mew`, 3, 50);

console.log(treecko.pound(mew));
console.log(treecko.pound(mew));